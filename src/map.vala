/* map.vala
 *
 * Copyright 2018 Robert Roth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Platformer {

    public const int FPS = 60;
    public class Image : GLib.Object {
        public string source {get; set construct; }
        public int width {get; set construct; }
        public int height {get; set construct; }

        public Image.from_element (GXml.DomElement element) {
            Object (width: int.parse (element.get_attribute ("width")),
                    height: int.parse (element.get_attribute ("height")),
                    source: element.get_attribute ("source"));
        }
    }

    public struct Point {
        public int x;
        public int y;
        public Point (int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public class Layer: GLib.Object {
        public string name {get; set construct; }
        public int width {get; set construct; }
        public int height {get; set construct; }
        public string encoding {get; set construct; }
        public bool visible {get; set construct; }
        public bool locked {get; set construct; }
        public int[,] tile_data;

        public Layer.from_element(GXml.DomElement element) {
            var enc = element.first_element_child.get_attribute ("encoding");
            var layer_width = int.parse (element.get_attribute ("width"));
            var layer_height = int.parse (element.get_attribute ("height"));
            var data = element.first_element_child.first_child.node_value;
            string[] split_data = data.split (",", layer_height * layer_width);
            int[,] layer_data = new int[layer_height, layer_width];
            for (int x = 0; x < layer_width; x++) {
                for (int y = 0; y < layer_height; y++) {
                    layer_data[y, x] = int.parse(split_data[y * layer_width + x]);
                }
            }
            bool is_visible = !element.has_attribute("visible") || (element.get_attribute ("visible") != "0");
            bool is_locked = element.has_attribute("locked") && (element.get_attribute ("locked") == "1");
            Object (width: layer_width,
                    height: layer_height,
                    name: element.get_attribute ("name"),
                    encoding: enc,
                    visible: is_visible,
                    locked: is_locked);

            tile_data = layer_data;
        }
    }

    public class Map : GLib.Object {
        public int width {get; set construct; }
        public int height {get; set construct; }
        public int tile_width {get; set construct; }
        public int tile_height {get; set construct; }
        public string background_color {get; set construct; }
        public Layer[] layers;
        public TileSet[] tilesets;
        public MapObject[] map_objects;

        public Map.from_element (GXml.DomElement element) {
            var layer_data = element.get_elements_by_tag_name ("layer");
            Layer[] local_layers = new Layer[layer_data.length];
            for (int i=0;i<layer_data.length;i++) {
                local_layers[i] = new Layer.from_element (layer_data.item(i));
            }

            var tileset_data = element.get_elements_by_tag_name ("tileset");
            TileSet[] local_tilesets = new TileSet[tileset_data.length];
            for (int i=0;i<tileset_data.length;i++) {
                local_tilesets[i] = new TileSet.from_element (tileset_data.item(i));
            }

            var object_data = element.get_elements_by_tag_name ("object");
            MapObject[] local_objects = new MapObject[object_data.length];
            for (int i=0;i<object_data.length;i++) {
                local_objects[i] = new MapObject.from_element (object_data.item(i));
            }

            Object (width: int.parse (element.get_attribute ("width")),
                    height: int.parse (element.get_attribute ("height")),
                    tile_width: int.parse (element.get_attribute ("tilewidth")),
                    tile_height: int.parse (element.get_attribute ("tileheight")),
                    background_color: element.get_attribute ("backgroundcolor"));
            layers = local_layers;
            tilesets = local_tilesets;
            map_objects = local_objects;
        }

        public Gee.List get_tiles_at (int row, int column) {
            Gee.List<int> tiles = new Gee.ArrayList<int>();
            foreach (Layer layer in layers) {
                if (layer.visible && layer.locked && layer.tile_data[column, row] != 0) {
                    tiles.add(layer.tile_data[column,row]);
                }
            }
            // warning ("Getting tiles at %d, %d returns %d: %d", row, column, tiles.size, tiles.size>0?tiles.get(0):0);
            return tiles;
        }

        public TileSet find_tileset_for_gid(int gid) {
            int max_before = 0;
            TileSet? tileset = null;
            for (int i=0;i<tilesets.length;i++) {
                if (gid > tilesets[i].first_gid && tilesets[i].first_gid > max_before) {
                    max_before = tilesets[i].first_gid;
                    tileset = tilesets[i];
                }
            }
            return tileset;
        }

        public void draw_background_to (Cairo.Context context, int startx, int starty, int endx, int endy) {
            foreach (Layer layer in layers) {
                draw_layer (context, layer,
                            startx, starty,
                            endx, endy);
            }
        }

        private void draw_layer (Cairo.Context context, Layer layer,
                                 int startx, int starty,
                                 int endx, int endy) {
            if (!layer.visible)
                return;
            TileCache tile_cache = TileCache.instance ();
            for (int row = starty; row < endy; row ++)
                for (int column = startx; column < endx; column ++)
                    if (layer.tile_data[row, column] != 0 && tile_cache.has_key (layer.tile_data[row, column])) {
                        var tile = tile_cache.@get (layer.tile_data[row, column]);
                        int positionx = (int) (column * tile_width);
                        int positiony = (int) (row * tile_height);
                        tile.draw_to (context, positionx, positiony + tile_height);
                    }
        }
    }
}