namespace Platformer {
        public const int TOP = 0;
        public const int BOTTOM = 1;
        public const int LEFT = 2;
        public const int RIGHT = 3;
        public const int IDLE_PLAYER_TILE = 256;
        public const int MOVING_PLAYER_TILE = 259;

        public class MapObject : GLib.Object {
        public int width {get; set; }
        public int height {get; set; }
        public int x {get; set; }
        public int y {get; set; }
        public string name {get; set construct; }
        public string object_type {get; set construct; }
        public weak Tile tile {get; set; }
        public int id {get; set construct; }
        private int current_frame_index = 0;
        private uint next_frame_timeout = 0;
        private uint motion_timeout = 0;
        public bool visible {get;set;}
        public bool blocked {get;set;}
        public Point direction {get;set;}

        public signal void queue_redraw_area (int x, int y, int width, int height);
        public signal GLib.Object check_collision (MapObject object, int x, int y);
        public signal int can_move_horizontally (MapObject object, int move_x);
        public signal int can_move_vertically (MapObject object, int move_y);
        public signal void position_updated (int new_x, int new_y);

        public MapObject.from_element(GXml.DomElement element) {
            Object (id: int.parse(element.get_attribute ("id")),
                    width: int.parse (element.get_attribute ("width")),
                    height: int.parse (element.get_attribute ("height")),
                    x: int.parse (element.get_attribute ("x")),
                    y: int.parse (element.get_attribute ("y")),
                    name: element.get_attribute ("name"),
                    object_type: element.get_attribute ("type"),
                    tile: TileCache.instance ().@get (int.parse (element.get_attribute("gid"))),
                    visible: false,
                    blocked:false);
        }

        private bool next_frame_for_object () {
            current_frame_index = (current_frame_index+1)%tile.frames.length;
            queue_redraw_area (x, y-height, width, height);
            next_frame_timeout = 0;
            return GLib.Source.REMOVE;
        }

        private bool move_object (Cairo.Context context) {
            Point current_direction = Point (direction.x, direction.y);
            current_direction.x = direction.x > 0 ? int.min (direction.x, can_move_horizontally(this, direction.x))
                                                  : int.max (direction.x, can_move_horizontally(this, direction.x));
            current_direction.y = direction.y > 0 ? int.min (direction.y, can_move_vertically(this, direction.y))
                                                  : int.max (direction.y, can_move_vertically(this, direction.y));

            if (object_type=="enemy" && current_direction.x * direction.x <= 0) {
                current_direction.x = (-1)*direction.x;
                direction = Point(current_direction.x, direction.y);
            }
            if (check_collision(this, x+current_direction.x, y+current_direction.y) == null) {
                x += current_direction.x;
                y += current_direction.y;
                if (visible == true) {
                    redraw_object (context, x, y-height);
                    queue_redraw_area (x - current_direction.x.abs(), y - height - current_direction.y.abs (),
                                       width + 2*current_direction.x.abs(), height + 2*current_direction.y.abs ());
                }
                position_updated (x, y);
                return GLib.Source.CONTINUE;
            } else {
                blocked = true;
                motion_timeout = 0;
                return GLib.Source.REMOVE;
            }

        }

        private void redraw_object (Cairo.Context context, int xpos, int ypos) {
            Gdk.Pixbuf pixbuf;
            if (object_type == "player") {
                if (direction.x == 0 && (tile.tileset.first_gid + tile.id) != IDLE_PLAYER_TILE) {
                    tile = TileCache.instance ().@get(IDLE_PLAYER_TILE);;
                    current_frame_index = 0;
                } else if (direction.x != 0 && (tile.tileset.first_gid + tile.id) != MOVING_PLAYER_TILE) {
                    tile = TileCache.instance ().@get(MOVING_PLAYER_TILE);;
                    current_frame_index = 0;
                }
            }

            if (tile.frames == null) {
                pixbuf = tile.pixbuf;
            } else {
                var current_frame = tile.frames[current_frame_index];
                pixbuf = TileCache.instance ().@get(tile.tileset.first_gid + current_frame.tile_id).pixbuf;
            }
            if (direction.x < 0) {
                pixbuf = pixbuf.flip (true);
            }
            Gdk.cairo_set_source_pixbuf (context, pixbuf, xpos, ypos);
            context.rectangle (xpos, ypos, pixbuf.width, pixbuf.height);
            context.fill ();

            // draw collision detection boundary
            /*context.set_source_rgb (1.0, 0, 0);
            context.rectangle (xpos+width/8, ypos+height/8, width*3/4, height*3/4);
            context.stroke ();*/
        }

        public void draw_to (Cairo.Context context, int xpos, int ypos) {
            if (tile.frames != null && next_frame_timeout == 0) {
                next_frame_timeout = GLib.Timeout.add_full (GLib.Priority.DEFAULT, tile.frames[current_frame_index].duration, next_frame_for_object);
            }
            if (motion_timeout == 0 && !blocked) {
                motion_timeout = GLib.Timeout.add_full (GLib.Priority.DEFAULT, 1000/FPS, () =>
                                                        {return move_object (context);});
            }
            if (visible == true) {
                redraw_object (context, xpos, ypos);
            }
        }
    }

}