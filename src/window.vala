/* window.vala
 *
 * Copyright 2018 Robert Roth
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Platformer {
	[GtkTemplate (ui = "/org/gnome/Platformer/window.ui")]
	public class Window : Gtk.ApplicationWindow {

		Gtk.DrawingArea game_area;

		[GtkChild]
		Gtk.Viewport viewport;

		[GtkChild]
		Gtk.Overlay overlay;

		public Window (Gtk.Application app) {
			Object (application: app);
			try {
			    var stream = GLib.resources_open_stream("/org/gnome/Platformer/level1.tmx", GLib.ResourceLookupFlags.NONE);
			    GXml.GomDocument document = new GXml.GomDocument.from_stream (stream);
			    var css_provider = new Gtk.CssProvider();
			    css_provider.load_from_resource ("/org/gnome/Platformer/window.css");
			    Gtk.StyleContext.add_provider_for_screen (Gdk.Screen.get_default(), css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                maximize ();
			    var map = new Map.from_element(document.document_element);
			    game_area = new MapView.for_viewport(map, viewport);
			} catch (GLib.Error e) {
			    error ("Error loading level from stream %s".printf(e.message));
			}
		}
	}
}
