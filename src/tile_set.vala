namespace Platformer {

    public class Frame: GLib.Object {
        public int tile_id {get; set construct; }
        public int duration {get; set construct; }

        public Frame.from_element (GXml.DomElement element) {
            Object (tile_id: int.parse(element.get_attribute("tileid")),
                    duration: int.parse(element.get_attribute("duration")));
        }
    }

    public class TileCache : Gee.HashMap<int, Tile> {
        private static GLib.Once<TileCache> _instance;

        public static unowned TileCache instance () {
            return _instance.once (() => { return new TileCache (); });
        }
    }

    public class Tile: GLib.Object {
        public int id {get; set construct; }
        public Image? image {get; set construct; }
        public Frame[]? frames;
        public weak TileSet tileset { get; set construct; }
        public Gdk.Pixbuf pixbuf {get; set construct; }

        public Tile.from_element (GXml.DomElement element, TileSet tileset_param) {
            Object (id: int.parse (element.get_attribute("id")),
                    image: new Image.from_element (element.get_elements_by_tag_name ("image").item (0)),
                    tileset: tileset_param);
            TileCache.instance ().@set((id+tileset.first_gid), this);

            var frames_data = element.get_elements_by_tag_name ("frame");
            if (frames_data.length > 0) {
                warning ("Loading %d frames for tile for %s", frames_data.length, image.source);
                Frame[] local_frames = new Frame[frames_data.length];
                for (int i=0;i<frames_data.length;i++) {
                    local_frames[i] = new Frame.from_element (frames_data.item(i));
                }
                frames = local_frames;
            }

            if (image != null) { // tilesets with separate images
                try {
                    pixbuf = new Gdk.Pixbuf.from_resource("/org/gnome/Platformer/"+image.source);
                } catch (GLib.Error err) {
                    warning ("Error loading tile image from resource %s: %s", image.source, err.message);
                }
            }

        }

        public Tile.from_tileset (TileSet tileset_param, int id_param) {
            Object (id: id_param,
                    tileset: tileset_param);
            var pos = tileset.get_tile_position (id+tileset.first_gid);
            assert (tileset.pixbuf != null);
            pixbuf =  new Gdk.Pixbuf.subpixbuf(tileset.pixbuf, pos.x, pos.y, tileset.tile_width, tileset.tile_height);
            TileCache.instance ().@set((id+tileset.first_gid), this);
        }

        public void draw_to (Cairo.Context context, int posx, int posy) {
            if (pixbuf != null) {
                int positiony = posy - pixbuf.height;
                Gdk.cairo_set_source_pixbuf (context, pixbuf, posx, positiony);
                context.rectangle (posx, positiony, pixbuf.width, pixbuf.height);
                context.fill();
            }
        }
    }

    public class TileSet: GLib.Object {
        public int first_gid {get; set construct; }
        public string name {get; set construct; }
        public int tile_width {get; set construct; }
        public int tile_height {get; set construct; }
        public int tile_count {get; set construct; }
        public int spacing {get; set construct; }
        public int columns {get; set construct; }
        public Image image {get; set construct; }
        public Tile[] tiles;
        public Gdk.Pixbuf pixbuf {get; set construct; }

        public TileSet.from_element (GXml.DomElement element) {
            Object (first_gid: int.parse(element.get_attribute ("firstgid")),
                    tile_width: int.parse (element.get_attribute ("tilewidth")),
                    tile_height: int.parse (element.get_attribute ("tileheight")),
                    tile_count: int.parse (element.get_attribute ("tilecount")),
                    columns: int.parse (element.get_attribute ("columns")),
                    name: element.get_attribute ("name"),
                    spacing : (element.get_attribute ("spacing") == null ? 0 : int.parse (element.get_attribute ("spacing"))),
                    image: new Image.from_element (element.get_elements_by_tag_name ("image").item (0)));
            Tile[] local_tiles = null;
            try {
                pixbuf = new Gdk.Pixbuf.from_resource("/org/gnome/Platformer/"+image.source);
            } catch (GLib.Error err) {
                warning ("Error loading tileset %s: %s", name, err.message);
            }
            if (columns == 0) {
                var tile_data = element.get_elements_by_tag_name ("tile");
                local_tiles = new Tile[tile_data.length];
                for (int i=0;i<tile_data.length;i++) {
                    local_tiles[i] = new Tile.from_element (tile_data.item(i), this);
                }
            } else {
                local_tiles = new Tile[tile_count];
                for (int i=0; i < tile_count;i++) {
                    local_tiles[i] = new Tile.from_tileset (this, i);
                }
            }
            tiles = local_tiles;

        }

        public Point get_tile_position(int gid) {
            assert (columns != 0);
            assert (gid>=first_gid);
            int internal_pos = gid - first_gid;
            int posx = internal_pos%columns;
            int posy = internal_pos/columns;
            return Point( (tile_width + spacing)* posx, (tile_height + spacing) * posy );
        }

        public int get_tile_index(int gid) {
            assert (columns == 0);
            assert (gid>=first_gid);
            int internal_pos = gid - first_gid;
            return internal_pos;
        }
    }
}