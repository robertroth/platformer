namespace Platformer {

    public class MapView : Gtk.DrawingArea {
        private Map _map_model;

        private MapObject player;

        // drawing tips
        // https://www.retrogames.cc/nes-games/super-mario-bros-japan-usa.html
        public Map map_model {
            get {
                return this._map_model;
            }
            set construct {
                this._map_model = map_model;
            }
        }
        private float scaling_factor = 1f;
        private const int PRELOAD = 200;
        private int previous_x = 0;
        private Gtk.Viewport viewport;
        private uint32 last_keypress;
        private uint last_keyval;

        public int can_move_horizontally_cb (MapObject object, int move_x) {
            // in case no motion, don't move
            if (move_x == 0) return move_x;
            // player should not be able to move backwards
            if (object.object_type == "player" && ((object.x - viewport.hadjustment.value).abs() < -move_x)) return 0;

            // enemies turn around
            if (object.object_type != "player") {
                // if at the start of the screen, turn around
                if (object.x <= 5) return (-1)*move_x;
                // if at the end of the level, turn around
                if (object.x > map_model.tile_width * map_model.width) return (-1)*move_x;
            }

            // in case of moving forward
            if (move_x > 0) {
                // if far away from tile limits, then move
                if (map_model.tile_width - (object.x + object.width)%map_model.tile_width > move_x)
                    return move_x;
                else {
                    // if close to the tile limit, make sure we can move to the next tile
                    if (map_model.get_tiles_at ((object.x+object.width)/map_model.tile_width+1, object.y/map_model.tile_height-1).is_empty)
                        return move_x;
                    else // if the next tile is blocking, don't allow motion
                        return 0;
                }
            } else if (move_x < 0) { // in case moving backwards
                // in case far away from tile limits, just move
                if (object.x%map_model.tile_width > move_x.abs()) return move_x;
                // otherwise check if the previous tile is blocking
                else if (map_model.get_tiles_at(object.x/map_model.tile_width-1, object.y/map_model.tile_height-1).is_empty)
                    return move_x;
                else // if blocking, allow to move to the tile limit, but not further
                    return - object.x%map_model.tile_width;
            }
            return 0;
        }

        public int can_move_vertically_cb (MapObject object, int move_y) {
            if (move_y == 0) return move_y;
            return 10*move_y/move_y.abs();
        }

        private GLib.Object check_collision_cb (MapObject moving, int x, int y) {
            return null;
            Gdk.Rectangle moving_rect = Gdk.Rectangle ();
            moving_rect.x = x;
            moving_rect.y = y;
            moving_rect.width = moving.width;
            moving_rect.height = moving.height;
            foreach (var map_object in map_model.map_objects) {
                if (map_object != moving) {
                    Gdk.Rectangle other = Gdk.Rectangle ();
                    other.x = map_object.x;
                    other.y = map_object.y;
                    other.width = map_object.width;
                    other.height = map_object.height;
                    if (moving_rect.intersect (other, null)) {
                        warning ("Object %d and %d met", map_object.id, moving.id);
                        return map_object;
                    }
                }

            }
            return null;
        }

        public void scroll_with_player_cb (int x, int y, Gtk.Viewport viewport) {
            if (x > previous_x && (x - viewport.hadjustment.value) > viewport.get_allocated_width ()/3 ) {
                viewport.hadjustment.value = x - viewport.get_allocated_width ()/3;
            }
            previous_x = x;

            // warning ("Position updated to %d, min value is %.2f, max value is %.2f, current value is %.2f, alloc width is %d",x, viewport.hadjustment.lower, viewport.hadjustment.upper, viewport.hadjustment.value, viewport.get_allocated_width ());
        }

        public MapView.for_viewport (Map map, Gtk.Viewport viewport) {
            _map_model = map;
            set_size_request ((int)(map.width*map.tile_width*scaling_factor),
                              (int)(map.height*map.tile_height*scaling_factor));
            this.draw.connect (draw_cb);
            set_halign (Gtk.Align.CENTER);
            set_valign (Gtk.Align.END);
            var color = map.background_color;
			var css = " * { background-color : %s; font: 1.5em \"Sans\"; }".printf(color);
			var provider = new Gtk.CssProvider ();
			try {
			    provider.load_from_data (css);
			} catch (GLib.Error e) {
			    warning ("Error loading level stylesheet " + e.message);
			}
			viewport.get_style_context ().add_provider (provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            this.show ();
			viewport.add (this);
			this.viewport = viewport;
			foreach (var map_object in map.map_objects) {
			    // map_object.queue_redraw_area.connect ((x,y, width, height)=> { queue_draw_area (x - PRELOAD, y - PRELOAD, width + 2*PRELOAD, height+2*PRELOAD);});
			    map_object.check_collision.connect(check_collision_cb);
			    map_object.can_move_horizontally.connect (can_move_horizontally_cb);
                if (map_object.object_type == "player") {
                    map_object.direction = Point (0, 0);
                    map_object.position_updated.connect ((x,y)=>scroll_with_player_cb(x, y, viewport));
                    player = map_object;
                } else if (map_object.object_type == "enemy") {
                    map_object.direction = Point (-3, 0);
                }
			}
			this.set_can_default (true);
			this.set_can_focus (true);
			this.grab_default ();
			this.key_press_event.connect(key_pressed_cb);
			this.key_release_event.connect(key_released_cb);
			GLib.Timeout.add_full (GLib.Priority.DEFAULT, 1000/FPS, () => {queue_draw(); return GLib.Source.CONTINUE;});
        }

        public bool key_pressed_cb (Gdk.EventKey event) {
            if (event.keyval == last_keyval)
              return true;
            warning ("Key %u pressed at %u", event.keyval, event.time);

            last_keypress = event.time;
            switch (event.keyval) {
                case Gdk.Key.Right:
                    player.direction = Point(5,0);
                    last_keyval = event.keyval;
                    break;
                case Gdk.Key.Left:
                    player.direction = Point(-5,0);
                    last_keyval = event.keyval;
                    break;
            }

            return true;
        }

        public bool key_released_cb (Gdk.EventKey event) {
            warning ("Key %u released at %u with last press at %u", event.keyval, event.time, last_keypress);
            if (event.time > last_keypress && last_keyval == event.keyval) // only process if no other key is pressed
            {
              player.direction = Point(0,0);
              last_keyval = 0;
            }

            return true;
        }

        public bool draw_cb (Cairo.Context context) {
            context.scale (scaling_factor, scaling_factor);
            // draw_grid (context);
            Gdk.Rectangle rect = Gdk.Rectangle ();
            if (Gdk.cairo_get_clip_rectangle (context, out rect)) {
                rect.x= rect.x - PRELOAD;
                rect.y= rect.y - PRELOAD;
                rect.width = rect.width + 2*PRELOAD;
                rect.height = rect.height + 2*PRELOAD;

                draw_background (context, rect);
                draw_objects (context, rect);
            }
            return false;
        }

        private void draw_background (Cairo.Context context, Gdk.Rectangle rect) {
            int startx = (int)(rect.x / map_model.tile_width).clamp (0, map_model.width);
            int starty = (int)(rect.y / map_model.tile_height).clamp (0, map_model.height);
            int endx = (int) (startx + rect.width / map_model.tile_width).clamp (0, map_model.width);
            int endy = (int) (starty + rect.height / map_model.tile_height).clamp (0, map_model.height);
            map_model.draw_background_to (context, startx, starty, endx, endy);
        }

        private void draw_objects (Cairo.Context context, Gdk.Rectangle rect) {
            foreach (var map_object in map_model.map_objects) {
                Gdk.Rectangle object_rect = Gdk.Rectangle ();
                int xpos =  map_object.x;
                int ypos =  map_object.y - map_object.height;

                object_rect.x = xpos;
                object_rect.y = ypos;
                object_rect.width = map_object.width;
                object_rect.height = map_object.height;
                if (object_rect.intersect(rect, null)) {
                    map_object.visible = true;
                    map_object.draw_to (context, xpos, ypos);
                }
            }
        }

        private void draw_grid (Cairo.Context context) {
			int height = map_model.tile_height * map_model.height;
			int width = map_model.tile_width * map_model.width;
            context.set_source_rgb (0.5, 0.7, 0.8);
            for (int column = 1; column < _map_model.width; column++ ) {
                int position = (int)(column*_map_model.tile_width);
                context.move_to (position, 0);
                context.line_to (position, height);
            }

            for (int row = 1; row < _map_model.height; row++ ) {
                int position = (int)(row*_map_model.tile_height);
                context.move_to (0, position);
                context.line_to (width, position);
            }

			context.set_line_width (1);
			context.stroke ();
        }
    }
}
